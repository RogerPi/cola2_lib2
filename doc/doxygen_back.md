# COLA2 LIB

Reusable code that is used all over the COLA2 software architecture with  (c++ & python).

## Documentation

Go back to the main documentation page [link](../index.html)

@defgroup comms comms
@brief Encode/decode functions to transmit data over acoustic communications.

@defgroup io io
@brief Input output classes and functions for network and serial communications.

@defgroup mission mission
@brief Classes and functions to define, read and write missions.

@defgroup utils utils
@brief Classes and functions related to angles, errors, files, NED, pressure conversions, threading and other miscellaneous topics.

@defgroup wmm wmm
@brief Functions related to the World Magnetic Model.
